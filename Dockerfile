from alpine:3

label org.opencontainers.image.authors="danielkinsman@riseup.net"
label org.opencontainers.licenses="MIT"
label org.opencontainers.image.source="https://gitlab.com/DanielKinsman/game-server-list"
label org.opencontainers.image.url="https://gitlab.com/DanielKinsman/game-server-list"
label org.opencontainers.version="0.0.1"


run apk add --no-cache py3-gunicorn py3-flask

volume /var/game-server-list
env SERVER_LIST_DB=/var/game-server-list/servers.db
env WEB_CONCURRENCY="4"

copy app.py /opt/app.py
workdir /opt

cmd python3 app.py && gunicorn -b 0.0.0.0:80 app:app
