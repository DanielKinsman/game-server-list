The canonical repository for this codebase is https://gitlab.com/DanielKinsman/game-server-list
Please raise any issues there.


# Game Server List

A quick and dirty REST API server list for your game.

By default servers are only valid for 5 minutes at which point they have to register themselves again. This
can be overridden with an environment variable `SERVER_EXPIRY="+5 minutes"`.

Allows anyone in the world to add a server to the list, so probably not the most secure thing in the world.
Run it as a docker container and be sure to put a TLS reverse proxy in front of it. To clean up old entries
in the database you can restart the container or otherwise periodically exec

    python3 /opt/app.py

inside the container.

## Rest API routes

### GET `/servers/<version>`

Get the current list of servers for <version> of the game. (e.g. "/servers/1.1.1"). Example response:

    {
        "servers": [
            {"host": "agameserver.com", "port": 51234, "players": 0, "max_players": 10, "password_protected": false},
            {"host": "11.22.33.44", "port": 51234, "players": 0, "max_players": 10, "password_protected": true}
        ]
    }

### PUT `/servers/<version>`

Add or update and existing server to the list of servers running that version of the game. Payload example:

    {
        "host": "1.1.1.1",
        "port": 51234,
        "players": 0,
        "max_players" 10,
        "password_protected": false
    }
