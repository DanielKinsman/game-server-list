#!/usr/bin/env python

import flask
from flask import request
import os
import sqlite3


app = flask.Flask(__name__)


def db() -> sqlite3.Connection:
    return sqlite3.connect(os.getenv("SERVER_LIST_DB", "servers.db"))


def expiry() -> str:
    """return server expiry time in sqlite time mod format"""
    return os.getenv("SERVER_EXPIRY", "+5 minutes")


def create_table():
    con = db()
    try:
        with con:
            con.execute(
                """
                create table if not exists servers
                (
                    host varchar,
                    port integer,
                    version varchar,
                    players integer,
                    max_players integer,
                    password_protected integer,
                    expiration timestamp,
                    primary key (host, port)
                )
                """
            )
    finally:
        con.close()


def update(
    host: str,
    port: int,
    version: str,
    players: int = 0,
    max_players: int = 0,
    password_protected: bool = False,
):
    con = db()
    try:
        with con:
            con.execute(
                """insert or replace into servers
                    (host, port, expiration, version, players, max_players, password_protected)
                    values (?, ?, datetime('now', 'utc', ?), ?, ?, ?, ?)""",
                (
                    host,
                    port,
                    expiry(),
                    version,
                    players,
                    max_players,
                    password_protected,
                ),
            )
    finally:
        con.close()


def delete_expired():
    con = db()
    try:
        with con:
            con.execute(
                "delete from servers where expiration <= datetime('now', 'utc')"
            )
    finally:
        con.close()


@app.route("/servers/<version>", methods=["GET", "PUT"])
def servers(version: str):
    if request.method == "PUT":
        update(
            request.form["host"],
            request.form["port"],
            version,
            request.form.get("players", 0),
            request.form.get("max_players", 0),
            request.form.get("password_protected", False),
        )
        return {}, 200
    else:
        con = db()
        try:
            cur = con.cursor()
            query = (
                "select host, port, players, max_players, password_protected"
                " from servers where expiration > datetime('now', 'utc') and version = ?"
            )
            return {
                "servers": [
                    {
                        "host": row[0],
                        "port": row[1],
                        "players": row[2],
                        "max_players": row[3],
                        "password_protected": bool(row[4]),
                    }
                    for row in cur.execute(query, (version,))
                ]
            }
        finally:
            con.close()


if __name__ == "__main__":
    # If running as a script instead of via flask, do db maintenance stuff
    # Can run as a cron job to regularly trim the db size
    create_table()
    delete_expired()
